Model: Logan
Versiune: MCV 1.0 SCe
Generatie: MCV
Anul fabricatiei: 2019
Kilometraj: 9 100 km
Combustibil: Benzina
Putere: 73 CP
Capacitate cilindrica: 998 cm3
Transmisie: Fata
Cutie de viteze: Manuala
Norma de poluare: Euro 6
Emisii CO2: 123 g/km
Caroserie: Combi
Numar de portiere: 5
Culoare: Negru
