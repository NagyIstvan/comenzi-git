Versiune: 1.3 CDTI
Anul fabricatiei: 2012
Kilometraj: 239 000 km
VIN: W0L0SDL08C6016885
Combustibil: Diesel
Putere: 75 CP
Capacitate cilindrica: 1 248 cm3
Transmisie: Fata
Cutie de viteze: Manuala
Norma de poluare: Euro 5
Filtru de particule: Da
Caroserie: Masina mica
Numar de portiere: 3
Culoare: Negru
